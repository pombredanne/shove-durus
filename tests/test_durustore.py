from stuf.six import PY3, unittest
from tests.test_store import Store

if not PY3:

    class TestDurusStore(Store, unittest.TestCase):

        initstring = 'durus://test.durus'

        def tearDown(self):
            import os
            self.store.close()
            os.remove('test.durus')